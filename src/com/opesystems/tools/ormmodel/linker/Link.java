package com.opesystems.tools.ormmodel.linker;

import java.util.List;

public class Link {
	private Class<?> parent;
	private Class<?> child;
	private String name;

	public Link(Class<?> parent, Class<?> child, String name) {
		super();
		this.parent = parent;
		this.child = child;
		this.name = name;
	}

	public Class<?> getParent() {
		return parent;
	}

	public void setParent(Class<?> parent) {
		this.parent = parent;
	}

	public Class<?> getChild() {
		return child;
	}

	public void setChild(Class<?> child) {
		this.child = child;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static <L extends Link> L search(List<L> links, Class<?> parent, Class<?> child) {
		L link = null;
		for (Link link2 : links) {
			if (link2.parent.equals(parent) && link2.child.equals(child)) {
				link = (L) link2;
				break;
			}
		}
		return link;
	}
}
