package com.opesystems.tools.ormmodel.linker;

public class DeleteLink extends Link{

	protected boolean delete = true;
	public DeleteLink(Class<?> parent, Class<?> child, String name) {
		super(parent, child, name);
		// TODO Auto-generated constructor stub
	}
	
	public DeleteLink(Class<?> parent, Class<?> child, String name, boolean delete) {
		super(parent, child, name);
		this.delete = delete;
	}
	
	public boolean isDelete() {
		return delete;
	}

}
