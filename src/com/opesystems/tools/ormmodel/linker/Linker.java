//package com.opesystems.tools.ormmodel.linker;
//
//import java.lang.reflect.Field;
//
//import java.lang.reflect.ParameterizedType;
//import java.lang.reflect.Type;
//import java.lang.reflect.TypeVariable;
//import java.sql.SQLException;
//import java.util.Arrays;
//
//import com.j256.ormlite.field.DatabaseField;
//import com.j256.ormlite.stmt.DeleteBuilder;
//import com.j256.ormlite.stmt.QueryBuilder;
//import com.j256.ormlite.table.DatabaseTable;
//import com.opesystems.tools.ormmodel.db.DBHelper;
//import com.opesystems.tools.ormmodel.db.DBInfo;
//import com.opesystems.tools.ormmodel.db.dao.AbstractDao;
//
//import static com.opesystems.tools.ormmodel.db.DBHelper.*;
//
///**
// * The purpose of the class is detect the relations between the Class and its
// * fields in database context.<br/>
// * 
// */
//public class Linker {
//
//	protected AbstractDao dao;
//	protected final Class<? extends DBInfo> dbInfoClass;
//
//	public Linker(Class<? extends DBInfo> dbInfoClass) {
//		this.dbInfoClass = dbInfoClass;
//		dao = new AbstractDao(dbInfoClass);
//	}
//
//	public Class<? extends DBInfo> getDbInfoClass() {
//		return dbInfoClass;
//	}
//
//	public void deleteCascade(Class<?> classInstance, QueryBuilder<?, ?> qb,
//			DeleteLink... links) {
//		String idName = getIdName(classInstance);
//		getInstance(dbInfoClass).getWritableDatabase().beginTransaction();
//		Class<? extends Object> clazz = classInstance;
//
//		System.out.println(qb);
//		qb.selectColumns(idName);
//		delete(null, clazz, qb, links);
//
//		// DeleteBuilder<?, Object> db = dao.deleteBuilder(clazz);
//		//
//		// db.where().in(id.getName(), qb);
//
//		getInstance(dbInfoClass).getWritableDatabase()
//				.setTransactionSuccessful();
//		getInstance(dbInfoClass).getWritableDatabase().endTransaction();
//
//	}
//
//	public void deleteCascade(Object instance, DeleteLink... links) {
//		Id id = getId(instance);
//		try {
//			getInstance(dbInfoClass).getWritableDatabase().beginTransaction();
//			Class<? extends Object> clazz = instance.getClass();
//			QueryBuilder<?, Object> qb = dao.queryBuilder(clazz);
//
//			qb.selectColumns(id.getName());
//			qb.where().eq(id.getName(), id.getValue());
//
//			delete(null, clazz, qb, links);
//
//			// DeleteBuilder<?, Object> db = dao.deleteBuilder(clazz);
//			//
//			// db.where().in(id.getName(), qb);
//			getInstance(dbInfoClass).getWritableDatabase()
//					.setTransactionSuccessful();
//			getInstance(dbInfoClass).getWritableDatabase().endTransaction();
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	public boolean isDBPersisted(Class<?> clazz) {
//		boolean is = false;
//
//		is = clazz.getAnnotation(DatabaseTable.class) != null;
//		if (!is) {
//			Field[] fields = clazz.getDeclaredFields();
//			for (Field field : fields) {
//				is = field.getAnnotation(DatabaseField.class) != null;
//				if (is) {
//					break;
//				}
//			}
//		}
//		return is;
//	}
//
//	private void delete(Class<?> parent, Class<?> child, QueryBuilder<?, ?> qb,
//			DeleteLink... links) {
//		Class<?> clazz = child;
//		Field[] fields = clazz.getDeclaredFields();
//
//		// Construct the query for delete from this class and children
//
//		boolean delete = true;
//
//		DeleteLink l = Link.search(Arrays.asList(links), parent, child);
//		delete = l == null || l.isDelete();
//
//		if (delete) {
//			QueryBuilder<?, ?> iqb = null;
//			String idName = getIdName(clazz);
//			String foreignName = idName;
//			if (parent != null) {
//				Field parentField = getParentField(parent, child, links);
//
//				if (parentField != null) {
//
//					foreignName = getForeignName(parentField, "_id");
//
//				}
//			}
//			try {
//
//				iqb = dao.queryBuilder(clazz);
//				iqb.selectColumns(idName);
//				iqb.where().in(foreignName, qb);
//
//				// System.out.println("S  " + iqb.prepareStatementString());
//			} catch (SQLException e) {
//				e.printStackTrace();
//				return;
//
//			}
//
//			// Delete children
//			for (Field field : fields) {
//				boolean isNotForeign = !isForeign(field);
//				if (isNotForeign) {
//					Class<?> fClass = getType(field);
//					if (isDBPersisted(fClass)) {
//
//						delete(child, fClass, iqb, links);
//
//					}
//				}
//			}
//
//			try {
//
//				DeleteBuilder<?, Object> db = dao.deleteBuilder(clazz);
//				db.where().in(foreignName, qb);
//
//				System.out
//						.println(db.prepareStatementString() + "-> deleted: ");
//				// + db.delete());
//
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	public String getIdName(Class<?> clazz) {
//		String name = null;
//
//		Field[] fields = clazz.getDeclaredFields();
//		for (Field field : fields) {
//			DatabaseField ann = field.getAnnotation(DatabaseField.class);
//
//			if (ann != null) {
//				boolean id = ann.generatedId() | ann.id();
//				if (id) {
//					name = !ann.columnName().equals("") ? ann.columnName()
//							: field.getName();
//				}
//			}
//		}
//
//		return name;
//	}
//
//	public Id getId(Object instance) {
//		Id idValue = null;
//		Class<?> clazz = instance.getClass();
//		Field[] fields = clazz.getDeclaredFields();
//		for (Field field : fields) {
//			DatabaseField ann = field.getAnnotation(DatabaseField.class);
//
//			if (ann != null) {
//				boolean id = ann.generatedId() | ann.id();
//				if (id) {
//					try {
//						field.setAccessible(true);
//						Object value = field.get(instance);
//						idValue = new Id(
//								!ann.columnName().equals("") ? ann.columnName()
//										: field.getName(), value);
//					} catch (IllegalArgumentException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (IllegalAccessException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//
//			}
//		}
//		return idValue;
//	}
//
//	public Field getParentField(Class<?> parent, Class<?> child, Link... links) {
//		Field pField = null;
//		boolean hasLink = links.length > 0;
//		Link link = null;
//		if (hasLink) {
//			link = Link.search(Arrays.asList(links), parent, child);
//			hasLink = link != null;
//		}
//
//		Field[] fields = child.getDeclaredFields();
//		for (Field field : fields) {
//			if (isForeign(field)) {
//				Class<?> type = getType(field);
//
//				if (type.equals(parent)) {
//					if (!hasLink || hasName(field, link.getName(), "_id")) {
//						pField = field;
//						break;
//
//					}
//
//				}
//			}
//		}
//		return pField;
//	}
//
//	public boolean isForeign(Field field) {
//		boolean is = false;
//
//		DatabaseField ann = field.getAnnotation(DatabaseField.class);
//
//		if (ann != null) {
//			is = ann.foreign();
//		}
//
//		return is;
//	}
//
//	public String getForeignName(Field field, String suffix) {
//		String name = null;
//
//		DatabaseField ann = field.getAnnotation(DatabaseField.class);
//
//		if (ann != null) {
//			boolean is = ann.foreign();
//
//			if (is) {
//				name = !ann.foreignColumnName().equals("") ? ann
//						.foreignColumnName()
//						: !ann.columnName().equals("") ? ann.columnName()
//								: field.getName().concat(suffix);
//			}
//		}
//
//		return name;
//	}
//
//	public boolean hasName(Field field, String name, String defaultSuffix) {
//		boolean result = false;
//		String fname = field.getName();
//
//		result = fname.equals(name);
//		if (!result) {
//			DatabaseField ann = field.getAnnotation(DatabaseField.class);
//
//			if (ann != null) {
//
//				fname = !ann.foreignColumnName().equals("") ? ann
//						.foreignColumnName()
//						: !ann.columnName().equals("") ? ann.columnName()
//								: field.getName().concat(defaultSuffix);
//				result = fname.equals(name);
//
//			}
//		}
//		return result;
//	}
//
//	public void getType(Class<?> clazz) {
//		TypeVariable<?>[] tps = clazz.getTypeParameters();
//		for (TypeVariable<?> typeVariable : tps) {
//			System.out.println("el tp de " + clazz + " es " + typeVariable
//					+ ": " + Arrays.asList(typeVariable.getBounds()) + " "
//					+ typeVariable.getGenericDeclaration() + " "
//					+ typeVariable.getName());
//		}
//		if (tps.length > 0) {
//			System.out.println("el tp de " + clazz + " es " + tps[0]);
//
//		}
//
//	}
//
//	private Class<?> getType(Field field) {
//		Class<?> ctype = null;
//		try {
//			Type type = field.getGenericType();
//
//			if ((type instanceof ParameterizedType)) {
//				Type[] tArgs = ((ParameterizedType) type)
//						.getActualTypeArguments();
//				if (tArgs.length > 0) {
//					ctype = (Class<?>) tArgs[0];
//				}
//			} else {
//				ctype = (Class<?>) type;
//
//			}
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return ctype;
//	}
//}
