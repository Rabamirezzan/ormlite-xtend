package com.opesystems.tools.ormmodel.linker;

public class Node {
	private String fieldName;
	private Class<?> clazz;

	
	
	public Node() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Node(String fieldName, Class<?> clazz) {
		super();
		this.fieldName = fieldName;
		this.clazz = clazz;
	}



	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

}
