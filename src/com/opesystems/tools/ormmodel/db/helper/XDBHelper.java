package com.opesystems.tools.ormmodel.db.helper;

import android.content.Context;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.opesystems.tools.ormmodel.db.ClassesToStore;
import com.opesystems.tools.ormmodel.db.DBHelper;
import com.opesystems.tools.ormmodel.db.DBInstance;
import com.opesystems.tools.ormmodel.db.XOrmLiteSqliteOpenHelper;

public abstract class XDBHelper extends DBHelper {

	public static enum Platform {
		JAVA, ANDROID
	};

	private final DatabaseInfo info;

	private final Context context;
	private final AndroidDatabaseInfo androidInfo;
	
	private final Platform platform;

	public XDBHelper(DatabaseInfo info) {
		super();
		this.info = info;
		this.context = null;
		this.androidInfo = null;
		platform = Platform.JAVA;
	}
	
	public XDBHelper(Context context, AndroidDatabaseInfo info) {
		super();
		this.info = null;
		this.context = context;
		this.androidInfo = info;
		platform = Platform.ANDROID;
	}

	@Override
	public DBInstance newInstance(
			java.lang.Class<? extends ClassesToStore> dbInfoClass) {
		DBInstance instance = null;
		if(Platform.JAVA.equals(platform)) {
			instance = _newJavaInstance(dbInfoClass);
		}else if(Platform.ANDROID.equals(platform)){
			instance = _newAndroidInstance(dbInfoClass);
		}
		return instance;
	};

	
	
	protected DBInstance _newJavaInstance(
			java.lang.Class<? extends ClassesToStore> dbInfoClass) {
		final String DATABASE_URL = info.getUrl();
		DBInstance instance = null;
		try {
			ConnectionSource connectionSource = new JdbcConnectionSource(
					DATABASE_URL);
			instance = new DBInstance(connectionSource, dbInfoClass);

			instance.createAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instance;

	};
	
	protected DBInstance _newAndroidInstance(Class<? extends ClassesToStore> classesToStore) {

		return new XOrmLiteSqliteOpenHelper(context, androidInfo.getName(), null,
				androidInfo.getVersion(), classesToStore).getDBInstance();
	}

}
