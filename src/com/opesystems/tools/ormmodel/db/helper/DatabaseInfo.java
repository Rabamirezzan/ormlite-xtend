package com.opesystems.tools.ormmodel.db.helper;

public interface DatabaseInfo {
	public String getUrl();
}
