package com.opesystems.tools.ormmodel.db.helper;

import android.content.Context;

import com.opesystems.tools.ormmodel.db.ClassesToStore;
import com.opesystems.tools.ormmodel.db.DBHelper;
import com.opesystems.tools.ormmodel.db.DBInstance;
import com.opesystems.tools.ormmodel.db.XOrmLiteSqliteOpenHelper;

public abstract class AndroidXDBHelper extends DBHelper {

	private Context context;
	private AndroidDatabaseInfo info;

	public AndroidXDBHelper(Context context, AndroidDatabaseInfo info) {
		this.context = context;
		this.info = info;
	}

	@Override
	public DBInstance newInstance(Class<? extends ClassesToStore> classesToStore) {

		return new XOrmLiteSqliteOpenHelper(context, info.getName(), null,
				info.getVersion(), classesToStore).getDBInstance();
	}

	// public DBInstance getInstance() {
	//
	// return getInstance("ABC", DBClasses.class);
	//
	// }

}
