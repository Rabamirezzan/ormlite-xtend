package com.opesystems.tools.ormmodel.db.helper;

public interface AndroidDatabaseInfo {
	public String getName();

	public int getVersion();
}
