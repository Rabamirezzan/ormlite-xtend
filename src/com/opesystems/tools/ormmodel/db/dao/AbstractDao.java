//package com.opesystems.tools.ormmodel.db.dao;
//
//import java.lang.annotation.Annotation;
//import java.lang.reflect.Field;
//import java.lang.reflect.Modifier;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.atomic.AtomicInteger;
//import java.util.concurrent.atomic.AtomicReference;
//
//import android.content.Context;
//
//import com.j256.ormlite.dao.Dao;
//import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
//import com.j256.ormlite.field.DatabaseField;
//import com.j256.ormlite.stmt.DeleteBuilder;
//import com.j256.ormlite.stmt.QueryBuilder;
//import com.j256.ormlite.stmt.UpdateBuilder;
//import com.opesystems.ormfly.ORMFly;
//import com.opesystems.ormfly.ORMFlyDatabase;
//import com.opesystems.ormfly.impl.db.CursorDB;
//import com.opesystems.ormfly.utils.AttributeParser;
//import com.opesystems.tools.ormmodel.db.DBHelper;
//import com.opesystems.tools.ormmodel.db.DBInfo;
//import com.opesystems.tools.ormmodel.db.DBInstance;
//import com.opesystems.tools.ormmodel.db.exception.SQLRuntimeException;
//
///**
// * All methods of this class could throw the runtimeException SQLExceptionRTE
// * that is a SQLException wrapper.
// */
//
//public class AbstractDao {
//	// protected Dao<T, Integer> dao;
//	public static final int UPDATE_FIELDS_MODE = 1;
//	public static final int IGNORE_FIELDS_MODE = 2;
//
//	private static final int ONE = 1;
//	private static AtomicInteger counter;
//
//	private static boolean enabledCounter = false;
//
//	// protected Context context = null;
//	// protected final Class<? extends DBInfo> dbInfoClass;
//
//	static {
//		enabledCounter = false;
//	}
//
//	private DBInstance instance;
//
//	public AbstractDao(DBInstance instance) {
//		this.instance = instance;
//	}
//
//	public void close() {
//		instance.close();
//	}
//
//	/**
//	 * Could throw {@link SQLException}
//	 */
//
//	public <D extends com.j256.ormlite.dao.Dao<T, ?>, T extends Object> D getDao(
//			final java.lang.Class<T> clazz) {
//
//		final AtomicReference<D> aDao = new AtomicReference<D>();
//
//		D dao = instance.getDao(clazz);
//		aDao.set(dao);
//
//		return aDao.get();
//	}
//
//	/**
//	 * Could throws the runtimeException SQLExceptionRTE that is a SQLException
//	 * wrapper.
//	 * 
//	 * @param instance
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public <T> boolean save(T instance) {
//
//		boolean result = false;
//
//		try {
//			Dao<T, ?> dao = (Dao<T, ?>) this.getDao(instance.getClass());
//
//			CreateOrUpdateStatus status = dao.createOrUpdate(instance);
//			result = status.isCreated() || status.isUpdated();
//
//			incrementCounter();
//		} catch (SQLException e) {
//
//			throw new SQLRuntimeException(e);
//		}
//		return result;
//	}
//
//	/**
//	 * Could throws the runtimeException SQLExceptionRTE that is a SQLException
//	 * wrapper.
//	 * 
//	 * @param instance
//	 * @param mode
//	 * @param fields
//	 * @return
//	 */
//	public <T, S> boolean save(T instance, int mode, String... fields) {
//		boolean result = false;
//
//		List<String> list = Arrays.asList(fields);
//		result = save(instance, list, mode);
//
//		return result;
//	}
//
//	/**
//	 * Could throws the runtimeException SQLExceptionRTE that is a SQLException
//	 * wrapper.
//	 * 
//	 * @param instance
//	 * @param fields
//	 * @param mode
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public <T, S> boolean save(T instance, List<String> fields, int mode) {
//		boolean result = false;
//
//		try {
//			String fieldId = null;
//			// Class<S> fieldClass = null;
//			Object id = null;
//			try {
//				fieldId = getIdFieldName(instance);
//				// fieldClass = InstanceUtil.getFieldClass(instance);
//				id = getIdValue(instance);
//			} catch (Exception e) {
//
//				e.printStackTrace();
//			}
//			Dao<T, S> dao = (Dao<T, S>) getDao(instance.getClass());
//			if (fieldId == null) {
//				throw new SQLException("No field id on " + instance.getClass()
//						+ ", instance " + instance);
//			}
//
//			if (!dao.idExists((S) id)) {
//				dao.createIfNotExists(instance);
//			} else {
//				// System.out.println("update " + instance.getClass());
//				Map<String, Object> values = new HashMap<String, Object>();
//				try {
//					values = getFieldsMap(instance);
//				} catch (IllegalArgumentException e) {
//
//					e.printStackTrace();
//				} catch (IllegalAccessException e) {
//
//					e.printStackTrace();
//				}
//				UpdateBuilder<T, ?> updater = dao.updateBuilder();
//
//				updater.where().eq(fieldId, id);
//
//				List<String> ffields = new ArrayList<String>();
//
//				if (mode == IGNORE_FIELDS_MODE) {
//					for (String fi : fields) {
//						values.remove(fi);
//					}
//					ffields.addAll(values.keySet());
//				} else if (mode == UPDATE_FIELDS_MODE) {
//					ffields.addAll(fields);
//				}
//
//				for (String fi : ffields) {
//					try {
//						if (hasAnnotation(instance.getClass(), fi,
//								DatabaseField.class)) {
//							updater.updateColumnValue(fi, values.get(fi));
//						}
//					} catch (NoSuchFieldException e) {
//
//						e.printStackTrace();
//					}
//				}
//				result = updater.update() > 0;
//
//			}
//			incrementCounter();
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//		return result;
//	}
//
//	/**
//	 * Could throws the runtimeException SQLExceptionRTE that is a SQLException
//	 * wrapper.
//	 * 
//	 * @param instances
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public <T> boolean saveAll(T... instances) {
//
//		boolean result = true;
//
//		boolean isList = false;
//		if (instances.length > 0) {
//			isList = instances[0] instanceof List;
//			if (isList) {
//				result = saveAllList((List<T>) instances[0]);
//			}
//		}
//		if (!isList) {
//			for (T t : instances) {
//
//				result &= save(t);
//			}
//		}
//
//		return result;
//	}
//
//	/**
//	 * Could throws the runtimeException SQLExceptionRTE that is a SQLException
//	 * wrapper.
//	 * 
//	 * @param instances
//	 * @return
//	 */
//	public <T> boolean saveAllList(List<T> instances) {
//
//		boolean result = true;
//
//		for (T t : instances) {
//			save(t);
//		}
//		return result;
//	}
//
//	/**
//	 * Could throws the runtimeException SQLExceptionRTE that is a SQLException
//	 * wrapper.
//	 * 
//	 * @param instances
//	 * @param mode
//	 * @param fieldNames
//	 * @return
//	 */
//	public <T> boolean saveAllList(List<T> instances, int mode,
//			String... fieldNames) {
//
//		boolean result = true;
//
//		for (T t : instances) {
//			save(t, mode, fieldNames);
//		}
//		return result;
//	}
//
//	public <T> boolean remove(T... instances) {
//		List<T> list = Arrays.asList(instances);
//		return remove(list);
//	}
//
//	/**
//	 * Could throws the runtimeException SQLExceptionRTE that is a SQLException
//	 * wrapper.
//	 * 
//	 * @param instances
//	 * @return
//	 */
//
//	@SuppressWarnings("unchecked")
//	public <T, S> boolean remove(List<T> instances) {
//
//		boolean result = false;
//		try {
//			if (!instances.isEmpty()) {
//				T instance = instances.get(0);
//
//				Dao<T, S> dao = (Dao<T, S>) getDao(instance.getClass());
//
//				int count = dao.delete(instances);
//				result = count > 0;
//			}
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//		return result;
//	}
//
//	@SuppressWarnings("unchecked")
//	public <T> boolean refresh(T instance) {
//		boolean result;
//		try {
//			Dao<T, ?> dao = (Dao<T, ?>) getDao(instance.getClass());
//			result = dao.refresh(instance) >= ONE;
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//		return result;
//	}
//
//	public <T, S> T get(Class<T> clazz, S id) {
//
//		T t = null;
//		try {
//			Dao<T, S> dao = getDao(clazz);
//			t = dao.queryForId(id);
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//		return t;
//	}
//
//	@SuppressWarnings("unchecked")
//	public <T> List<T> getAll(Class<T> clazz) {
//		Dao<?, ?> dao = getDao(clazz);
//		List<T> list = new ArrayList<T>();
//		try {
//			list = (List<T>) dao.queryForAll();
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//
//		return list;
//	}
//
//	public <T> List<T> getAll(Class<T> clazz, String... selectColumns) {
//		Dao<T, ?> dao = getDao(clazz);
//		List<T> list = new ArrayList<T>();
//		try {
//			list = dao.queryBuilder().selectColumns(selectColumns).query();
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//
//		return list;
//	}
//
//	public <T> void delete(Class<T> clazz, T instance) {
//		Dao<T, ?> dao = getDao(clazz);
//		try {
//			dao.delete(instance);
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//	}
//
//	public <T, S> void delete(Class<T> clazz) {
//		Dao<T, S> dao = getDao(clazz);
//		DeleteBuilder<T, S> db = dao.deleteBuilder();
//
//		try {
//			db.delete();
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//		instance.clearCache(clazz);
//
//	}
//
//	public <T, S> void deleteById(Class<T> clazz, S id) {
//		Dao<T, S> dao = getDao(clazz);
//		try {
//			dao.deleteById(id);
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//	}
//
//	public <T> void delete(Class<T> clazz, List<T> instances) {
//		Dao<T, ?> dao = getDao(clazz);
//		try {
//			dao.delete(instances);
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//
//	}
//
//	@SuppressWarnings("unchecked")
//	public <T> void delete(T instance) {
//		Dao<T, ?> dao = (Dao<T, ?>) getDao(instance.getClass());
//		deleteCascade(instance);
//		try {
//			dao.delete(instance);
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		}
//	}
//
//	protected <T> void deleteCascade(T container) {
//		Field[] fields = container.getClass().getDeclaredFields();
//		for (Field field : fields) {
//			try {
//				if (field.getAnnotation(DatabaseField.class) != null) {
//					field.setAccessible(true);
//					deleteChild(field.get(container), container);
//				}
//			} catch (IllegalArgumentException e) {
//
//				e.printStackTrace();
//			} catch (IllegalAccessException e) {
//
//				e.printStackTrace();
//			}
//		}
//	}
//
//	protected <T, S> void deleteChild(T instance, S container)
//			throws IllegalArgumentException, IllegalAccessException {
//		Field[] fields = instance.getClass().getDeclaredFields();
//		boolean delete = false;
//		for (Field field : fields) {
//			ContainerReference cr = field
//					.getAnnotation(ContainerReference.class);
//			if (cr != null && cr.deleteOnContainerDelete()) {
//				field.setAccessible(true);
//				Object reference = field.get(instance);
//				if (reference != null) {
//					if (reference.equals(container)) {
//						delete = true;
//						break;
//					}
//				}
//			}
//		}
//		if (delete) {
//			delete(instance);
//		}
//	}
//
//	public void clearCache(Class<?> clazz) {
//		instance.clearCache(clazz);
//	}
//
//	public static void resetSaveCounter() {
//
//		if (enabledCounter && counter != null) {
//			counter.set(0);
//		}
//	}
//
//	public static void incrementCounter() {
//		if (enabledCounter && counter != null) {
//			synchronized (counter) {
//
//				counter.incrementAndGet();
//			}
//		}
//
//	}
//
//	public static void setCounter(AtomicInteger progress) {
//
//		if (enabledCounter) {
//			counter = progress;
//		}
//	}
//
//	public static boolean enableCounter(boolean e) {
//		return enabledCounter = e;
//	}
//
//	public static AtomicInteger getCounter() {
//
//		return counter;
//	}
//
//	@SuppressWarnings("unchecked")
//	public static <T> T getIdValue(Object identificable)
//			throws NoSuchFieldException, IllegalArgumentException,
//			IllegalAccessException {
//
//		Class<?> clazz = identificable.getClass();
//		Field[] fields = clazz.getDeclaredFields();
//		Object value = null;
//		for (Field field : fields) {
//			DatabaseField idAnn = field.getAnnotation(DatabaseField.class);
//			if (idAnn != null && (idAnn.id() || idAnn.generatedId())) {
//				field.setAccessible(true);
//				value = field.get(identificable);
//				break;
//			}
//		}
//		return (T) value;
//
//	}
//
//	public static String getIdFieldName(Object identificable)
//			throws NoSuchFieldException, IllegalArgumentException,
//			IllegalAccessException {
//
//		Class<?> clazz = identificable.getClass();
//		Field[] fields = clazz.getDeclaredFields();
//		String value = null;
//		for (Field field : fields) {
//			DatabaseField idAnn = field.getAnnotation(DatabaseField.class);
//			if (idAnn != null && (idAnn.id() || idAnn.generatedId())) {
//
//				// field.setAccessible(true);
//				value = field.getName();
//				break;
//			}
//		}
//		return value;
//
//	}
//
//	public static <T, S extends Annotation> boolean hasAnnotation(
//			Class<T> clazz, String fieldName, Class<S> annotation)
//			throws NoSuchFieldException {
//
//		Field f = clazz.getDeclaredField(fieldName);
//		f.setAccessible(true);
//
//		Object ann = f.getAnnotation(annotation);
//		boolean result = ann != null;
//
//		return result;
//	}
//
//	public Map<String, Object> getFieldsMap(Object instance)
//			throws IllegalArgumentException, IllegalAccessException {
//		Class<?> _class = instance.getClass();
//		Field[] fields = _class.getDeclaredFields();
//		Map<String, Object> map = new HashMap<String, Object>();
//		for (Field field : fields) {
//			// Exclude ann = field.getAnnotation(Exclude.class);
//			if (!Modifier.isTransient(field.getModifiers())
//					&& !Modifier.isStatic(field.getModifiers())) {
//				// && (ann==null||exclude)) {
//				field.setAccessible(true);
//				Object value = field.get(instance);
//
//				map.put(field.getName(), value);
//			}
//		}
//		return map;
//	}
//
//	/**
//	 * Could throw {@link SQLException}
//	 */
//	public <T, ID> QueryBuilder<T, ID> queryBuilder(Class<T> clazz) {
//		Dao<T, ID> dao = getDao(clazz);
//		return dao.queryBuilder();
//
//	}
//
//	/**
//	 * Could throw {@link SQLException}
//	 */
//	public <T, ID> DeleteBuilder<T, ID> deleteBuilder(Class<T> clazz) {
//		Dao<T, ID> dao = getDao(clazz);
//		return dao.deleteBuilder();
//	}
//
//	/**
//	 * Could throw {@link SQLException}
//	 */
//	public <T, ID> UpdateBuilder<T, ID> updateBuilder(Class<T> clazz) {
//		Dao<T, ID> dao = getDao(clazz);
//		return dao.updateBuilder();
//	}
//
//	ORMFly fly = new ORMFly();
//
//	public <V> void registerAttributeParser(Class<V> clazz,
//			Class<? extends AttributeParser<V>> parser) {
//		fly.registerAttributeParser(clazz, parser);
//	}
//
//	public <T> List<T> rawQuery(Class<T> clazz, String sql,
//			Object... selectionArgs) {
//
//		ORMFlyDatabase db = new CursorDB(instance.getReadableDatabase());
//		List<T> result = fly.rawQuery(db, clazz, sql, selectionArgs);
//		return result;
//	}
//
//	@SuppressWarnings("unchecked")
//	public <T> T rawQueryUnique(Class<T> clazz, T defaultValue, String sql,
//			Object... selectionArgs) {
//
//		List<T> result = rawQuery(clazz, sql, selectionArgs);
//		T e = null;
//		if (!result.isEmpty()) {
//			Object o = result.get(0);
//			e = (T) o;
//		} else {
//			e = defaultValue;
//		}
//		return e;
//	}
//
//}
