package com.opesystems.tools.ormmodel.db.dao;

public @interface ContainerReference {
boolean deleteOnContainerDelete() default true;
}
