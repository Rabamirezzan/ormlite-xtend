package com.opesystems.tools.ormmodel.db.tool;

import java.util.Iterator;
import java.util.List;

public class StringJoin {
	public static String join(List<String> datas, final String separator) {
		StringBuilder builder = new StringBuilder();
		Iterator<String> it = datas.iterator();
		while (it.hasNext()) {
			String data = (String) it.next();

			builder.append(data);
			if (it.hasNext()) {
				builder.append(separator);
			}
		}
		return builder.toString();
	}
}
