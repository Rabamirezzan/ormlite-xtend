package com.opesystems.tools.ormmodel.db;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.opesystems.tools.ormmodel.db.exception.SQLRuntimeException;

public class DBInstance {

	// private Class<? extends DBInfo> infoClass;
	private Class<? extends ClassesToStore> classes;

	// private String DATABASE_NAME = "";
	//
	// private int DATABASE_VERSION = 0;
	//
	private ConnectionSource source;

	private Map<Class<?>, Dao<?, ?>> map;

	{

		map = new HashMap<Class<?>, Dao<?, ?>>();

	}

	public DBInstance(ConnectionSource source,
			Class<? extends ClassesToStore> classes) {
		// super(context, databaseName, null, databaseVersion);
		// DATABASE_NAME = databaseName;
		// DATABASE_VERSION = databaseVersion;
		this.source = source;
		this.classes = classes;
		// create(null, source, info.getClassesToStore().getClasses());
	}

	// @Override
	// public void onCreate(SQLiteDatabase arg0, ConnectionSource cs) {
	//
	// create(arg0, cs, info.getClassesToStore().getClasses());
	//
	// }

	// @Override
	// public void onUpgrade(SQLiteDatabase db, ConnectionSource cs, int v1, int
	// v2) {
	//
	// drop(cs, info.getClassesToStore().getClasses());
	//
	// onCreate(db, cs);
	//
	// }

	protected void create(SQLiteDatabase arg0, ConnectionSource cs,
			Set<Class<?>> classes) {

		for (Class<?> class1 : classes) {
			try {
				TableUtils.createTableIfNotExists(cs, class1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	protected void drop(ConnectionSource cs, Set<Class<?>> classes) {
		try {
			// Log.i(DBHelper.class.getName(), "onUpgrade");
			// Todas las tablas
			for (Class<?> class1 : classes) {
				TableUtils.dropTable(cs, class1, true);
			}

			// onCreate(null, cs);
		} catch (SQLException e) {
			Log.e(DBHelper.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
		}
	}

	public void drop(Set<Class<?>> classes) {
		drop(source, classes);
	}

	public void dropAll() {
		dropAll(source);
	}

	public void dropAll(ConnectionSource source) {
		drop(source, ClassesToStoreTool.getClasses(classes));
	}

	public void createAll() {
		createAll(source);

	}

	public void createAll(ConnectionSource source) {
		create(null, source, ClassesToStoreTool.getClasses(classes));

	}

	/**
	 * Could throw {@link SQLException}
	 */
	@SuppressWarnings("unchecked")
	public <D extends com.j256.ormlite.dao.Dao<T, ?>, T extends Object> D getDao(
			java.lang.Class<T> clazz) {

		Dao<T, ?> dao = null;
		try {
			boolean contains = map.containsKey(clazz);

			if (!contains) {

				dao = DaoManager.createDao(source, clazz);
				dao.setObjectCache(true);
				dao.setObjectCache(ReferenceObjectCache.makeSoftCache());
				map.put(clazz, dao);
			} else {
				dao = (Dao<T, ?>) map.get(clazz);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);

		}

		return (D) dao;
	};

	@SuppressWarnings("unchecked")
	public <D extends com.j256.ormlite.dao.Dao<T, I>, T extends Object, I extends Object> D getDao(
			java.lang.Class<T> clazz, Class<I> classId) throws SQLException {

		Dao<T, I> dao = null;
		boolean contains = map.containsKey(clazz);

		if (!contains) {
			dao = DaoManager.createDao(source, clazz);
			dao.setObjectCache(true);
			dao.setObjectCache(ReferenceObjectCache.makeSoftCache());
			map.put(clazz, dao);
		} else {
			dao = (Dao<T, I>) map.get(clazz);
		}

		return (D) dao;
	};

	public <T> Dao<T, Integer> getDaoFor(T t) throws SQLException {

		T Dao = getDao(t.getClass());
		return extracted(Dao);
	}

	@SuppressWarnings("unchecked")
	private <T> Dao<T, Integer> extracted(T Dao) {
		return (com.j256.ormlite.dao.Dao<T, Integer>) Dao;
	}

	public boolean isOpen() {
		return source.isOpen();
	}

	public void shutdown() throws SQLRuntimeException {
		clearAllCache();
		try {
			source.close();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	public void clearCache(Class<?> clazz) {

		try {
			Dao<?, ?> dao = map.get(clazz);
			if (dao != null) {
				dao.clearObjectCache();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		map.remove(clazz);
	}

	public void clearAllCache() {
		Iterator<Class<?>> it = map.keySet().iterator();
		while (it.hasNext()) {
			try {
				Dao<?, ?> dao = map.get(it.next());
				if (dao != null) {
					dao.clearObjectCache();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		map.clear();
	}

	// public Context getContext() {
	//
	//
	// return dbcontext;
	// }
	//
	// protected boolean hasContext() {
	// return dbcontext != null;
	// }
	//
	// protected boolean isEqualContext(Context context) {
	// return hasContext() ? dbcontext.equals(context) : false;
	// }

}
