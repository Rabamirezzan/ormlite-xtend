package com.opesystems.tools.ormmodel.db.exception;

public class NoNameFieldCacheException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8423901976555324032L;

	public NoNameFieldCacheException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoNameFieldCacheException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

	public NoNameFieldCacheException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public NoNameFieldCacheException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	
}
