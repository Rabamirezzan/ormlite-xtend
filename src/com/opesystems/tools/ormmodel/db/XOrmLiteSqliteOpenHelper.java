package com.opesystems.tools.ormmodel.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.opesystems.tools.ormmodel.db.ClassesToStore;
import com.opesystems.tools.ormmodel.db.DBInstance;

public class XOrmLiteSqliteOpenHelper extends OrmLiteSqliteOpenHelper {

	// private Class<? extends ClassesToStore> store;

	private DBInstance instance;

	public XOrmLiteSqliteOpenHelper(Context context, String databaseName,
			CursorFactory factory, int databaseVersion,
			Class<? extends ClassesToStore> store) {
		super(context, databaseName, factory, databaseVersion);

		this.instance = new DBInstance(connectionSource, store);
		// this.store = store;
	}

	// {
	// ImmutableList<String> l = ImmutableList.<String> builder().build();
	// l.add("2");
	// }

	@Override
	public void onCreate(SQLiteDatabase database,
			ConnectionSource connectionSource) {
		instance.createAll(connectionSource);

	}

	@Override
	public void onUpgrade(SQLiteDatabase database,
			ConnectionSource connectionSource, int oldVersion, int newVersion) {
		instance.dropAll(connectionSource);
		instance.createAll(connectionSource);

	}

	public DBInstance getDBInstance() {

		return instance;
	}
}
