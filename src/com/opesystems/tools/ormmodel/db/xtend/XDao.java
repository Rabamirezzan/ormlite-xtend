package com.opesystems.tools.ormmodel.db.xtend;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.StatementBuilder;
import com.opesystems.tools.ormmodel.db.DBInstance;
import com.opesystems.tools.ormmodel.db.exception.SQLRuntimeException;

public class XDao<T, ID, W extends XWhere<?, ?, ?>> {

	private final DBInstance db;
	private final Class<T> cls;
	private final Class<W> whereClass;

	public XDao(DBInstance db, Class<T> modelClass, Class<W> whereClass) {
		this.db = db;
		this.cls = modelClass;
		this.whereClass = whereClass;
	}

	public W queryWhere() {
		Dao<T, ID> dao = db.getDao(cls);
		W where = getInstance(whereClass, dao.queryBuilder());
		return where;
	}

	public W updateWhere() {
		Dao<T, ID> dao = db.getDao(cls);
		W where = getInstance(whereClass, dao.updateBuilder());
		return where;
	}

	public W deleteWhere() {
		Dao<T, ID> dao = db.getDao(cls);
		W where = getInstance(whereClass, dao.deleteBuilder());
		return where;
	}

	public List<T> query() throws SQLRuntimeException{
		Dao<T, ID> dao = db.getDao(cls);
		List<T> result = new ArrayList<T>();
		try {
			result  = dao.queryBuilder().query();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		
		return result;
	}

	public static <T> T getInstance(Class<T> clazz, Object... params) {
		T result = null;
		try {
			Constructor<T> constructor = clazz
					.getDeclaredConstructor(StatementBuilder.class);
			result = constructor.newInstance(params);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * See {@link Dao#createOrUpdate(Object)}
	 * 
	 * @param model
	 */
	public void save(T model) throws SQLRuntimeException {

		try {
			db.getDao(cls).createOrUpdate(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}

	/**
	 * See {@link Dao#delete(Object)}
	 * 
	 * @param model
	 */
	public void delete(T model) throws SQLRuntimeException {

		try {
			db.getDao(cls).delete(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}

	/**
	 * See {@link Dao#delete(Collection)}
	 * 
	 * @param model
	 */
	public void delete(Collection<T> datas) throws SQLRuntimeException {

		try {
			db.getDao(cls).delete(datas);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}

	/**
	 * See {@link Dao#update(Object)}
	 * 
	 * @param model
	 */
	public void update(T model) throws SQLRuntimeException {
		try {
			db.getDao(cls).update(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * See {@link Dao#refresh(Object)}
	 * 
	 * @param model
	 */
	public void refresh(T model) {
		try {
			db.getDao(cls).refresh(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}
