package com.opesystems.tools.ormmodel.db.xtend;

public interface DaoInfo {
	public String globalDao();
	public String dbInfo();
}
