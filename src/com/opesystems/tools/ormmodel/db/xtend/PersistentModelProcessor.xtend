package com.opesystems.tools.ormmodel.db.xtend

import android.content.Context
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.stmt.Where
import com.opesystems.tools.ormmodel.db.ClassesToStore
import com.opesystems.tools.ormmodel.db.DBInstance
import com.opesystems.tools.ormmodel.db.helper.AndroidDatabaseInfo
import com.opesystems.tools.ormmodel.db.helper.DatabaseInfo
import com.opesystems.tools.ormmodel.db.helper.XDBHelper
import java.util.concurrent.atomic.AtomicReference
import org.eclipse.xtend.lib.macro.AbstractClassProcessor
import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.declaration.Visibility
import org.eclipse.xtend2.lib.StringConcatenationClient

import static extension com.opesystems.tools.ormmodel.db.xtend.DatabaseFieldTool.*

class PersistentModelProcessor extends AbstractClassProcessor {

	static val DAO_PACKAGE = "daoPackage"
	static val CLASSES_TO_STORE_CLASS = "classesToStoreClass"
	static val INTERNAL_CALL = "internalCall"

	override doRegisterGlobals(ClassDeclaration annotatedClass, extension RegisterGlobalsContext context) {
		println(annotatedClass)

		getWhereClassName(annotatedClass).registerClass
		getDaoClassName(annotatedClass).registerClass

		try {
			val classesToStore = getClassesToStore(annotatedClass)
			classesToStore?.registerClass
		} catch (Exception e) {
			e.printStackTrace
		}

		try {
			val ormHelperClass = annotatedClass.ormHelper

			ormHelperClass?.registerClass
		} catch (Exception e) {
		}
	}

	protected def getClassesToStore(ClassDeclaration annotatedClass) {
		val name = getXDaoPackageName(annotatedClass)

		//		name.packageName + ".XClassesToStore"
		name + ".XClassesToStore"
	}

	protected def getOrmHelper(ClassDeclaration annotatedClass) {
		val name = getXDaoPackageName(annotatedClass)
		name + ".OrmHelper"
	}

	protected def getPackageName(String className) {
		className.substring(0, className.lastIndexOf('.'))
	}

	protected def getXDaoPackageName(ClassDeclaration annotatedClass) {
		var String name = "com.opesystems.dao.NoDao"
		try {
			println(annotatedClass.annotations)
			val ann = annotatedClass.annotations.findFirst[a|a.annotationTypeDeclaration.qualifiedName == XDaoFun.name];

			//			println("x dao fun " + ann + " " + XDaoFun.name)
			val daoFactoryClass = ann?.getStringValue(DAO_PACKAGE);

			println("class in XDaoFun " + daoFactoryClass);
			name = if (!daoFactoryClass?.trim.nullOrEmpty) {
				daoFactoryClass;
			} else {
				val qname = annotatedClass.qualifiedName
				name = qname.packageName + ".DaosX"

			}

		} catch (Exception e) {
			e.printStackTrace
		}
		name
	}

	protected def addInternalDaoCall(MutableClassDeclaration annotatedClass) {
		val ann = annotatedClass.annotations.findFirst[a|a.annotationTypeDeclaration.qualifiedName == XDaoFun.name];
		var result = false
		result = ann?.getBooleanValue(INTERNAL_CALL);
		result
	}

	protected def getDaoClassName(ClassDeclaration annotatedClass) {
		val packageName = annotatedClass.getXDaoPackageName
		packageName + "." + annotatedClass.simpleName + "Dao"
	}

	protected def getWhereClassName(ClassDeclaration annotatedClass) {
		val packageName = annotatedClass.getXDaoPackageName
		packageName + "." + annotatedClass.simpleName + "Where"
	}

	def createOrmHelperClass(MutableClassDeclaration annotatedClass, MutableClassDeclaration storeClass,
		extension TransformationContext context) {
		val MutableClassDeclaration ormClass = findClass(annotatedClass.ormHelper)

		val constructor = ormClass.findDeclaredConstructor(DatabaseInfo.newTypeReference);
		if (constructor == null) {

			//			ormClass.addField("info")[
			//				type = DatabaseInfo.newTypeReference
			//			] 
			ormClass.extendedClass = XDBHelper.newTypeReference
			ormClass.addConstructor [
				addParameter("info", DatabaseInfo.newTypeReference)
				body = '''super(info);'''
			]

			ormClass.addConstructor [
				addParameter("context", Context.newTypeReference)
				addParameter("info", AndroidDatabaseInfo.newTypeReference)
				body = '''super(context, info);'''
			]

			ormClass.addMethod("getInstance") [
				body = '''return getInstance("unique_instance", XClassesToStore.class);'''
				returnType = DBInstance.newTypeReference
			]
		}
	}

	def createDaoXClass(MutableClassDeclaration annotatedClass, TypeReference idType, MutableClassDeclaration whereClass,
		extension TransformationContext context) {
		val MutableClassDeclaration daoClass = findClass(annotatedClass.daoClassName);

		val xDaoClass = XDao.newTypeReference(annotatedClass.newTypeReference, idType, whereClass.newTypeReference)
		daoClass.extendedClass = xDaoClass

		if (daoClass != null) {
			daoClass.addConstructor [
				addParameter("db", DBInstance.newTypeReference)
				addParameter("modelClass", Class.newTypeReference(annotatedClass.newTypeReference))
				addParameter("whereClass", Class.newTypeReference(whereClass.newTypeReference))
				body = '''super(db, modelClass, whereClass);'''
			]

			daoClass.addMethod("get") [
				static = true
				final = true
				visibility = Visibility.PUBLIC
				returnType = daoClass.newTypeReference
				addParameter("db", DBInstance.newTypeReference)
				body = '''«daoClass.simpleName» instance = new «daoClass.simpleName»(db,
				«annotatedClass.simpleName».class, «whereClass.simpleName».class);
				return instance;'''
			]

		}

		daoClass
	}

	static var StringConcatenationClient c = ''''''

	def createClassesToStoreClass(MutableClassDeclaration annotatedClass, extension TransformationContext context) {
		val MutableClassDeclaration storeClass = findClass(annotatedClass.classesToStore);
		if (!storeClass.implementedInterfaces.exists[i|i.simpleName == ClassesToStore.simpleName]) {
			storeClass.implementedInterfaces = storeClass.implementedInterfaces + #[ClassesToStore.newTypeReference]
		}

		//		var method = storeClass.findDeclaredMethod("getClasses")
		//  public Set<Class<?>> getClasses() ;
		//	public List<String> getTags(Class<?> cls);
		val _returnType = Class.newTypeReference(annotatedClass.newTypeReference)

		val field = storeClass.addField("model" + annotatedClass.simpleName + "Class") [
			initializer = ''' «annotatedClass.simpleName».class'''
			type = _returnType
			static = true
			final = true
			visibility = Visibility.PUBLIC
		]
		storeClass

	//		val method = storeClass.addMethod("get" + annotatedClass.simpleName + "Class") [
	//			body = '''return «annotatedClass.simpleName».class;'''
	//			returnType = _returnType
	//		]
	}

	def createWhereXClass(MutableClassDeclaration annotatedClass, TypeReference idType,
		MutableClassDeclaration whereClass, extension TransformationContext context) {
		val xWhereClass = XWhere.newTypeReference(annotatedClass.newTypeReference, idType,
			whereClass.newTypeReference)

		println("xwhere " + xWhereClass)

		whereClass.extendedClass = xWhereClass

		if (whereClass != null) {

			whereClass.addConstructor [
				addParameter("builder", StatementBuilder.newTypeReference(annotatedClass.newTypeReference, idType))
				body = '''super(builder);'''
			]

			val newClass = whereClass

			for (f : annotatedClass.declaredFields) {
				println(f.simpleName)

				if (f.containsDatabaseFieldAnnotation) {
					val name = f.name
					val foreign = f.foreign
					var _setForeignStatement = ""
					if (foreign) {
						_setForeignStatement = '''setForeignClass(«f.declaringType.simpleName».class);'''
					}
					val setForeignStatement = _setForeignStatement
					newClass.addMethod(f.simpleName) [
						docComment = '''{@link «f.type.name»} value.'''
						exceptions = exceptions + #[RuntimeException.newTypeReference]
						//addParameter("value", f.type)
						body = '''
							final String column = "«name»";
							setNameField(column);
							«setForeignStatement»							
							addOrderByColumn(column);
							return this;
						''';
						returnType = whereClass.newTypeReference
					]

				}
			}
		}
	}

	override doTransform(MutableClassDeclaration annotatedClass, extension TransformationContext context) {

		try {
			val idType = annotatedClass.idType;
			val MutableClassDeclaration whereClass = findClass(annotatedClass.whereClassName);

			createWhereXClass(annotatedClass, idType, whereClass, context)
			val daoClass = createDaoXClass(annotatedClass, idType, whereClass, context)
			val classesToStoreClass = createClassesToStoreClass(annotatedClass, context)

			createOrmHelperClass(annotatedClass, classesToStoreClass, context)
			if (annotatedClass.addInternalDaoCall) {
				annotatedClass.addMethod("getDao") [
					visibility = Visibility.PUBLIC
					static = true
					final = true
					addParameter("db", DBInstance.newTypeReference)
					body = '''return «daoClass.simpleName».get(db);'''
					returnType = daoClass.newTypeReference
				]
			}
		} catch (Exception e) {
			e.printStackTrace
		}
	}

	static def addWhereClause(MutableClassDeclaration newClass, extension TransformationContext context) {
		val whereClauseType = Where.newTypeReference
		newClass.addField("valueCache") [
			type = whereClauseType
			initializer = '''new «whereClauseType»'''
			primarySourceElement = newClass
		]
	}

	static def addValueCache(MutableClassDeclaration newClass, extension TransformationContext context) {
		val valueCacheType = AtomicReference.newTypeReference
		newClass.addField("valueCache") [
			type = valueCacheType
			initializer = '''new «valueCacheType»'''
			primarySourceElement = newClass
		]
	}

}
