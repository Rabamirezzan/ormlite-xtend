package com.opesystems.tools.ormmodel.db.xtend;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.opesystems.tools.ormmodel.db.exception.NoNameFieldCacheException;
import com.opesystems.tools.ormmodel.db.exception.SQLRuntimeException;
import com.opesystems.tools.ormmodel.db.tool.StringJoin;

@SuppressWarnings("all")
public class XWhere<T, ID, W extends XWhere> {

	private StatementBuilder<T, ID> builder;
	protected Where<T, ID> where;
	private AtomicReference<Class<?>> classFieldCache = new AtomicReference<Class<?>>();
	private AtomicReference<String> nameFieldCache = new AtomicReference<String>();
	private AtomicReference<List<String>> orderByCache = new AtomicReference<List<String>>();

	private AtomicBoolean foreignFieldCache = new AtomicBoolean();

	public XWhere(StatementBuilder<T, ID> builder) {
		this.builder = builder;

	}

	protected Where<T, ID> getWhere() {
		if (where == null) {
			this.where = builder.where();
		}
		return where;
	}

	// /**
	// * Shortcut for a new XWhere instance.
	// *
	// * @return
	// */
	// public W w() {
	// return (W) XDao.getInstance(this.getClass(), builder);
	// }

	protected String getNameField() throws NoNameFieldCacheException {
		String name = nameFieldCache.get();
		if (name == null) {
			throw new NoNameFieldCacheException("No nameField selected");
		} else {
			nameFieldCache.set(null);
		}
		return name;
	}

	protected boolean isForeign() {
		boolean result = false;

		result = classFieldCache.get() != null;
		return result;

	}

	protected Object getValueIfForeign(Object value) {
		Object result = value;
		if (isForeign() && classFieldCache.get().isInstance(value)) {
			result = DatabaseFieldTool.getIdValue(value);
			setForeignClass(null);
		}
		return result;

	}

	public W setForeignClass(Class<?> foreign) {
		classFieldCache.set(foreign);
		return (W) this;
	}

	public W setNameField(String name) {
		nameFieldCache.set(name);
		return (W) this;
	}

	public static <T, ID, W extends XWhere> Where<T, ID>[] get(
			XWhere<T, ID, W>... list) {
		List<Where<T, ID>> _others = new ArrayList<Where<T, ID>>();

		for (XWhere<T, ID, W> xWhere : list) {
			_others.add(xWhere.getWhere());
		}
		return (Where<T, ID>[]) _others.toArray(new Where[] {});
	}

	/**
	 * Get QueryBuilder.
	 * 
	 * @return
	 */
	public QueryBuilder<T, ID> qb() {
		QueryBuilder<T, ID> result = null;
		if (builder instanceof QueryBuilder) {
			result = ((QueryBuilder<T, ID>) builder);

		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					QueryBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * Get UpdateBuilder.
	 * 
	 * @return
	 */
	public UpdateBuilder<T, ID> ub() {
		UpdateBuilder<T, ID> result = null;
		if (builder instanceof UpdateBuilder) {
			result = ((UpdateBuilder<T, ID>) builder);

		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					UpdateBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * Get DeleteBuilder.
	 * 
	 * @return
	 */
	public DeleteBuilder<T, ID> db() {
		DeleteBuilder<T, ID> result = null;
		if (builder instanceof DeleteBuilder) {
			result = ((DeleteBuilder<T, ID>) builder);

		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					DeleteBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * See {@link Where#and()}
	 * 
	 * @return
	 */
	public W and() {
		getWhere().and();
		return (W) this;
	}

	/**
	 * See {@link Where#and(Where, Where, Where...)}
	 * 
	 * @param first
	 * @param second
	 * @param others
	 * @return
	 */
	public W and(W first, W second, W... others) {

		getWhere().and(first.getWhere(), second.getWhere(), get(others));
		return (W) this;
	}

	/**
	 * See {@link Where#between(String, Object, Object)}
	 * 
	 * @param low
	 * @param high
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W between(Object low, Object high) throws SQLRuntimeException,
			NoNameFieldCacheException {
		try {
			getWhere().between(getNameField(), low, high);
		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#countOf()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public long countOf() throws SQLRuntimeException, NoNameFieldCacheException {
		long result = 0;
		try {
			result = getWhere().countOf();
		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		}
		return result;
	}

	/**
	 * See {@link Where#eq(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W eq(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		Object _value = getValueIfForeign(value);
		try {
			getWhere().eq(getNameField(), _value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#ge(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W ge(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().ge(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#gt(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W gt(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().gt(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#idEq(Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W idEq(ID value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().idEq(value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#in(String, Object...)}
	 * 
	 * @param obs
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <S> XWhere<T, ID, W> in(S... obs) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().in(getNameField(), obs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#in(String, Iterable)}
	 * 
	 * @param obs
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <S> XWhere<T, ID, W> in(Iterable<S> obs) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().in(getNameField(), obs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#in(String, QueryBuilder)}
	 * 
	 * @param query
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W in(QueryBuilder<?, ?> query) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().in(getNameField(), query);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#isNotNull(String)}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W isNotNull() throws SQLRuntimeException, NoNameFieldCacheException {

		try {
			getWhere().isNotNull(getNameField());
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#isNull(String)}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W isNull() throws SQLRuntimeException, NoNameFieldCacheException {

		try {
			getWhere().isNull(getNameField());
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#le(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W le(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().le(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#like(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W like(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().like(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#lt(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W lt(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().lt(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#ne(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W ne(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().ne(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#not()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W not() throws SQLRuntimeException, NoNameFieldCacheException {

		getWhere().not();

		return (W) this;
	}

	/**
	 * See {@link Where#not(Where)}
	 * 
	 * @param where
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W not(XWhere<T, ID, W> where) throws SQLRuntimeException,
			NoNameFieldCacheException {

		this.getWhere().not(where.getWhere());

		return (W) this;
	}

	/**
	 * See {@link Where#notIn(String, Iterable)}
	 * 
	 * @param objects
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <S> XWhere<T, ID, W> notIn(Iterable<S> objects)
			throws SQLRuntimeException, NoNameFieldCacheException {

		try {
			getWhere().notIn(getNameField(), objects);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#notIn(String, Object...)}
	 * 
	 * @param objects
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <S> XWhere<T, ID, W> notIn(S... objects) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().notIn(getNameField(), objects);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#notIn(String, QueryBuilder)}
	 * 
	 * @param query
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W notIn(QueryBuilder<?, ?> query) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().notIn(getNameField(), query);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (W) this;
	}

	/**
	 * See {@link Where#or()}
	 */
	public W or() throws SQLRuntimeException, NoNameFieldCacheException {

		getWhere().or();

		return (W) this;
	}

	/**
	 * See {@link Where#or(Where, Where, Where...)}
	 * 
	 * @param first
	 * @param second
	 * @param others
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public W or(W first, W second, W... others) throws SQLRuntimeException,
			NoNameFieldCacheException {

		getWhere().or(first.getWhere(), second.getWhere(), get(others));

		return (W) this;
	}

	/**
	 * See {@link QueryBuilder#query()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public List<T> query() throws SQLRuntimeException,
			NoNameFieldCacheException {

		List<T> result;
		try {
			result = qb().query();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return result;
	}

	/**
	 * See {@link QueryBuilder#queryForFirst()}
	 * 
	 * @param defaultValue
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public T queryForFirst(T defaultValue) throws SQLRuntimeException,
			NoNameFieldCacheException {
		T result = null;
		try {
			result = qb().queryForFirst();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		if (result == null) {
			result = defaultValue;
		}
		return result;
	}

	/**
	 * Shortcut to {@link UpdateBuilder#update()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 */
	public int update() throws SQLRuntimeException {
		int result = 0;
		if (builder instanceof UpdateBuilder) {
			try {
				result = ((UpdateBuilder<T, ID>) builder).update();
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					UpdateBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * Shortcut to {@link DeleteBuilder#delete()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 */
	public int delete() throws SQLRuntimeException {
		int result = 0;
		if (builder instanceof DeleteBuilder) {
			try {
				result = ((DeleteBuilder<T, ID>) builder).delete();
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					DeleteBuilder.class.getName()));
		}
		return result;
	}

	public String getPrepareStatementString() throws SQLRuntimeException {
		String result = "";
		try {
			result = builder.prepareStatementString();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return result;
	}

	protected static boolean isOrderBy(AtomicReference<List<String>> columns) {
		return columns.get() != null;
	}

	protected void addOrderByColumn(String column) {
		if (isOrderBy(orderByCache)) {
			orderByCache.get().add(String.format("`%s`", column));
		}
	}

	public W orderBy() {
		orderByCache.set(new ArrayList<String>());
		return (W) this;
	}

	protected String getOrderBy(String order) {
		List<String> columns = orderByCache.get();

		String result = StringJoin.join(columns, ",");
		return result.concat(" ").concat(order);
	}

	protected void resetOrderByCache() {
		orderByCache.set(null);
	}

	public W desc() {
		String raw = getOrderBy("DESC");
		qb().orderByRaw(raw);
		resetOrderByCache();
		return (W) this;
	}

	public W asc() {
		String raw = getOrderBy("ASC");
		qb().orderByRaw(raw);
		resetOrderByCache();
		return (W) this;
	}

}
