package com.opesystems.tools.ormmodel.db;

import java.util.HashMap;
import java.util.Map;

public abstract class DBHelper {

	private static Map<String, DBInstance> instances;

	public static Map<String, DBInstance> getInstances() {
		if (instances == null) {
			instances = new HashMap<String, DBInstance>();
		}
		return instances;
	}

	public static void finish(String dbId) {

		try {

			DBInstance instance = instances.get(dbId);
			DBHelper.finish(instance);
			instances.remove(dbId);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	// private static Context context;
	//
	// public static Context getContext() {
	// return context;
	// }
	//
	// public static void setContext(Context context) {
	// DBHelper.context = context;
	// }

	// public static synchronized DBInstance getInstance(
	// Class<? extends DBInfo> dbInfoClass) {
	// return getInstance(context, dbInfoClass);
	// }

	// public static synchronized DBInstance newInstance(
	// Class<? extends DBInfo> dbInfoClass) {
	// return newInstance(context, dbInfoClass);
	// }

	public abstract DBInstance newInstance(
			Class<? extends ClassesToStore> classesToStore);

	public abstract DBInstance getInstance();
	public synchronized DBInstance getInstance(String dbId,
			Class<? extends ClassesToStore> info) {

		DBInstance instance = null;

		boolean createNew = true;
		if (getInstances().containsKey(dbId)) {
			instance = getInstances().get(dbId);

			createNew = !instance.isOpen();
		}
		if (createNew) {
			instance = newInstance(info);
			getInstances().put(dbId, instance);
		}

		return instance;

	}

	public static void finish(DBInstance instance) {

		instance.shutdown();

	}

	public static void clearCache(DBInstance instance, Class<?> clazz) {

		instance.clearCache(clazz);
	}

	public static void clearAllCache(DBInstance instance) {

		instance.clearAllCache();
	}

	// public static void clearCache(Class<? extends DBInfo> dbInfoClass,
	// Class<?> clazz) {
	//
	// DBInstance instance = getInstance(dbInfoClass);
	// instance.clearCache(clazz);
	// }
	//
	// public static void clearAllCache(Class<? extends DBInfo> dbInfoClass) {
	//
	// DBInstance instance = getInstance(dbInfoClass);
	// instance.clearAllCache();
	// }

}
