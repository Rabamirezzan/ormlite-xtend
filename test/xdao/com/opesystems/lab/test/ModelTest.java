package com.opesystems.lab.test;

import com.j256.ormlite.field.DatabaseField;
import com.opesystems.tools.ormmodel.db.DBInstance;
import com.opesystems.tools.ormmodel.db.xtend.XDao;

public class ModelTest {
	

	@DatabaseField(generatedId = true)
	public int id;
	@DatabaseField(columnName="thisIsACustomName")
	public String name;
	
	@DatabaseField(columnName="edad")
	public int age;
	
	
	public static ModelTestDao getDao(DBInstance instance) {
		return ModelTestDao.getDao(instance);
	}
}
