package com.opesystems.lab.test;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.opesystems.tools.ormmodel.db.ClassesToStore;
import com.opesystems.tools.ormmodel.db.DBHelper;
import com.opesystems.tools.ormmodel.db.DBInfo;
import com.opesystems.tools.ormmodel.db.DBInstance;

public class XDaos {
	private static ModelTestDao modelTest;
	private static DBHelper helper;

	public static void setHelper(DBHelper helper) {
		XDaos.helper = helper;
	}

	protected static DBInstance getInstance() {

		DBInstance instance = helper.getInstance("RRB", XClassesToStore.class);

		return instance;
	}

	public static ModelTestDao getModelTestDao() {
		
		if (modelTest == null || !getInstance().isOpen()) {
			modelTest = new ModelTestDao(getInstance(), ModelTest.class,
					ModelTestWhere.class);
		}
		return modelTest;
	}
}
