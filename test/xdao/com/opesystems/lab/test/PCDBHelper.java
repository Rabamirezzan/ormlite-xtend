package com.opesystems.lab.test;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.opesystems.tools.ormmodel.db.ClassesToStore;
import com.opesystems.tools.ormmodel.db.DBHelper;
import com.opesystems.tools.ormmodel.db.DBInstance;

public class PCDBHelper extends DBHelper {

	@Override
	public DBInstance newInstance(
			java.lang.Class<? extends ClassesToStore> dbInfoClass) {
		final String DATABASE_URL = "jdbc:sqlite:C:/temp/xdaotest.db";
		DBInstance instance = null;
		try {
			ConnectionSource connectionSource = new JdbcConnectionSource(
					DATABASE_URL);
			instance = new DBInstance(connectionSource, dbInfoClass);
			instance.dropAll();
			instance.createAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instance;

	};

}
