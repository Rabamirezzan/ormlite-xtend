package com.opesystems.lab.test;

import com.opesystems.tools.ormmodel.db.DBHelper;
import com.opesystems.tools.ormmodel.db.DBInstance;
import com.opesystems.tools.ormmodel.db.xtend.XDao;

public class ModelTestDao extends XDao<ModelTest, Integer, ModelTestWhere> {

	public ModelTestDao(DBInstance db, Class<ModelTest> modelClass,
			Class<ModelTestWhere> whereClass) {
		super(db, modelClass, whereClass);
		// TODO Auto-generated constructor stub
	}

//	private static ModelTestDao instance;

//	protected static DBInstance getInstance(DBHelper helper) {
//
//		DBInstance instance = helper.getInstance("RRB", DBInfoTest.class);
//
//		return instance;
//	}
//
//	public static ModelTestDao getDao(DBHelper helper) {
//		DBInstance db = getInstance(helper);
//
//		ModelTestDao instance = new ModelTestDao(db, ModelTest.class,
//				ModelTestWhere.class);
//
//		return instance;
//	}

	public static ModelTestDao getDao(DBInstance db) {

		ModelTestDao instance = new ModelTestDao(db, ModelTest.class,
				ModelTestWhere.class);

		return instance;
	}

}
