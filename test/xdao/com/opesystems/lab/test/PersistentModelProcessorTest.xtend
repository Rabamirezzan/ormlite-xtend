package com.opesystems.lab.test

import com.opesystems.tools.ormmodel.db.xtend.XDaoFun
import org.eclipse.xtend.core.compiler.batch.XtendCompilerTester
import org.junit.Test

class PersistentModelProcessorTest {
	extension XtendCompilerTester compilerTester = XtendCompilerTester.newXtendCompilerTester(XDaoFun)

	@Test
	def test() {

		assertCompilesTo(
			'''
		import com.opesystems.tools.ormmodel.db.xtend.XDaoFun
		import com.j256.ormlite.field.DatabaseField
		import com.opesystems.tools.ormmodel.db.xtend.DaoInfo
		import com.opesystems.lab.test.Constants

		class Abc implements DaoInfo{
			public static final val HOLA='abc'
			override dbInfo() {
				"hola mundo"
			}
			
			override globalDao() {
				"yes dao"
			}
			
		}
		//class HolaMundo(){
			
		//	def ok(){}
		//}
		
		@XDaoFun(classesToStoreClass=Constants::CLS, internalCall=true)		
		class HelloWorld{
			@DatabaseField(columnName="theOmniPresentName")
			private String name = "this is the initialization"
			@DatabaseField(id=true)
			private Long x
			def abc(){}
		}
		
		@XDaoFun(classesToStoreClass=Constants::CLS)		
		class RayX{
			@DatabaseField(columnName="theOmniPresentName")
			private String rayLaser = "this is the initialization"
			@DatabaseField(id=true)
			private Integer identificator
		
		}
		''',
			'''
				''')
	}
}
