package com.opesystems.lab.test;

import junit.framework.Assert;

import org.junit.Test;

public class XDaoTest {
	@Test
	public void testName() throws Exception {

//		XDaos.setHelper();
		ModelTestDao dao = ModelTest.getDao(new PCDBHelper().getInstance("rb", XClassesToStore.class));

		String expected = "raul";
		ModelTest model = new ModelTest();
		model.name = expected;
		model.age = 22;
		dao.save(model);

		model = new ModelTest();
		model.name = expected;
		model.age = 33;
		dao.save(model);

		ModelTestWhere where = dao.queryWhere();// .name().eq(expected).and().age().ge(33);
		where.or(where.name().like("%%").and().age().eq(33), where.age()
				.between(0, 40));
		long count1 = where.orderBy().name().age().asc().countOf();
		String s = where.getPrepareStatementString();
		System.out.println(String.format("%s", s));

		Assert.assertEquals("ModelTest", 2, count1);

		int value = dao.deleteWhere().name().eq(expected).delete();
		Assert.assertEquals("deleted", 2, value);

	}
}
