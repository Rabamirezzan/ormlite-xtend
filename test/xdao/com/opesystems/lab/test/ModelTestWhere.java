package com.opesystems.lab.test;

import com.j256.ormlite.stmt.StatementBuilder;
import com.opesystems.tools.ormmodel.db.xtend.XWhere;

public class ModelTestWhere extends XWhere<ModelTest, Integer, ModelTestWhere> {

	public ModelTestWhere(StatementBuilder<ModelTest, Integer> builder) {
		super(builder);
		// TODO Auto-generated constructor stub
	}

	public ModelTestWhere name() {
		final String column = "thisIsACustomName";
		setNameField(column);
		addOrderByColumn(column);
		return this;
	}

	public ModelTestWhere age() {
		final String column = "edad";
		setNameField(column);
		addOrderByColumn(column);
		return this;
	}

}
